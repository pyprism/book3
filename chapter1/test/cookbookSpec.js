describe('MainCtrl', function () {
	var mainCtrl, $scope = {};
	beforeEach(module('cookbook'));
	beforeEach(inject(function ($controller) {
		$controller('MainCtrl', {
			$scope: $scope
		});
	}));
	it('should assign the correct rapper to scope', function () {
		expect($scope.emcee).toEqual('Kool G Rap');
	});
});